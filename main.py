#Methods
def toUpperCase(letter):
    return chr(ord(letter) - 32)
    
def toLowerCase(letter):
    return chr(ord(letter) + 32)

def isAlpha(letter):
    decimalVal = ord(letter)
    if (65 <= decimalVal <= 90) or (97 <= decimalVal <= 122):
        return True
    return False
    
def isDigit(digit):
    for char in digit:
        decimalVal = ord(char)
        if (decimalVal < 48) or (decimalVal > 57):
            return False
    return True

def isSpecialChar(letter):
    decimalVal = ord(letter)
    if (32 <= decimalVal <= 47) or (58 <= decimalVal <= 64) or (91 <= decimalVal <= 96) or (123 <= decimalVal <= 126):
        return True
    return False


# Test Cases
a = 'a'
B = 'B'
c = 'c'
nonAlpha = '$'
digit = '28'
nonDigit = '83#'
specialChar = '@'
nonSpecialChar = 'A'

print(f'Upper of {a} is {toUpperCase(a)}')
print(f'Lower of {B} is {toLowerCase(B)}')
print(f'Is {c} in the alphabet? {isAlpha(c)}')
print(f'Is {nonAlpha} in the alphabet? {isAlpha(nonAlpha)}')
print(f'Is {digit} a digit? {isDigit(digit)}')
print(f'Is {nonDigit} a digit? {isDigit(nonDigit)}')
print(f'Is {specialChar} a special character? {isSpecialChar(specialChar)}')
print(f'Is {nonSpecialChar} a special character? {isSpecialChar(nonSpecialChar)}')
