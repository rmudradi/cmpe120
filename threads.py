from threading import Thread
import time
import logging

def loop1():
    # counts from 0 to 10
    for i in range(0, 11, 1):
        print(i)
        time.sleep(1)
    
def loop2():
    # counts from 10 to 0
    for i in range(10, -1, -1):
        print(i)
        time.sleep(1)
            
if __name__ == '__main__':
    T1 = Thread(target=loop1, args=())
    T2 = Thread(target=loop2, args=())
    T1.start()
    T1.join()
    T2.start()
    T2.join()

